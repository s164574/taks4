#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: simple string calculator
  I want to use this template for my feature file

  @tag1
  Scenario: Adding an empty string
    Given a calculator
    And given an empty string
    When add is called
    Then 0 is returned

  @tag2
  Scenario: Addin a singel number
    Given a calculator
    And the string "2"
    When add is called
    Then it returns 2
    
   @tag3
  Scenario: Adding two numbers
    Given a the string "2,2"
    When when add is called with the calculator on the string
    Then it should return 4
    
    @tag4
  Scenario: Adding three numbers
    Given a the string "2,2,3"
    When when add is called with the calculator on the string
    Then it should return 7
    
     @tag5
  Scenario: Adding numbers in string seperate by new line
    Given a the string "2,2\n3"
    When when add is called with the calculator on the string
    Then it should return 7
    @tag6
  Scenario: Adding numbers in string seperate by a mix of new lines and commas
    Given a the string "2\n3\n2"
    When when add is called with the calculator on the string
    Then it should return 7
    
    


