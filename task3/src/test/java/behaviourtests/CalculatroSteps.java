package behaviourtests;

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import task3.Calculator;

public class CalculatroSteps {
	String input;
	int result;
	
	
	@Given("^a calculator$")
	public void aCalculator() throws Throwable {
	    Calculator c = new Calculator();
	}

	@Given("^given an empty string$")
	public void givenAnEmptyString() throws Throwable {
	    input ="";
	}

	@When("^add is called$")
	public void addIsCalled() throws Throwable {
	     result= new Calculator().add(input);
	}

	@Then("^(\\d+) is returned$")
	public void isReturned(int res2) throws Throwable {
	    assertEquals(res2,result);
	}

	@Given("^the string \"([^\"]*)\"$")
	public void theString(String num2) throws Throwable {
	    input=num2;
	}
	
	@Then("^it returns (\\d+)$")
	public void itReturns(int res1) throws Throwable {
	    assertEquals(res1,result);
	}
	
	@Given("^a the string \"([^\"]*)\"$")
	public void aTheString(String num) throws Throwable {
	    input = num;
	}

	@When("^when add is called with the calculator on the string$")
	public void whenAddIsCalledWithTheCalculatorOnTheString() throws Throwable {
		result= new Calculator().add(input);
	}

	@Then("^it should return (\\d+)$")
	public void itShouldReturn(int res) throws Throwable {
	    assertEquals(res,result);
	}

}
